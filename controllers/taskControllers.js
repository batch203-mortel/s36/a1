// import the Task model in our controllers. So that our controllers have access to our Task Model(method, schema)
const Task = require("../models/Task.js");

// Syntax app.httpMethods("/endpoint", (anonymousFunc));

// Create task: create a task without duplicate names.

module.exports.createTask = (request, response) =>{
	// checking captured data from the request body.
	console.log(request.body);

	Task.findOne({name: request.body.name}).then(result => {
		console.log(result);

		if( result != null && result.name == request.body.name){
			return response.send("Duplicate task found")
		}
		else{
			let newTask = new Task({
				name : request.body.name
			})
		}

		newTask.save()
		.then(result => response.send(result))
		.catch(error => response.send(error));
	})
	.catch(error => response.send(error));
}

// Retrieve ALL tasks

module.exports.getAllTaskController = (request, response) => {
	Task.find({})
	.then(tasks => response.send(tasks))
	.catch(error => response(error));
}

// Retrieve ALL tasks
module.exports.getSingleTaskController = (request, response) => {
	console.log(request.params);

	Task.findById(request.params.id)
	.then(result => response.send(result))
	.catch(erro => response.send(error));
}

// Updating task status
module.exports.updateTasksStatusController = (request, response) => {
	console.log(request.params.id);
	console.log(request.body);
	let updates = {
		status: request.body.status
	}
	// Syntax: findByIdAndUpdate(_id, {ObjectUpdate}, options)
		//{new:true}
	Task.findByIdAndUpdate(request.params.id, updates, {new: true})
	.then(updatedTask => response.send(updatedTask))
	.catch(error => response.send(error));
}

module.exports.deleteTask = (request, response)=>{
	console.log(request.params.id);

	Task.findByIdAndRemove(request.params.id)
	.then(removedTask => response.send(removedTask))
	.catch(error => response.send(error));
}

