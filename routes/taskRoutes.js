const express = require("express");
// express.Router() method allows access to HTTP methods.
const router = express.Router();

const taskControllers = require("../controllers/taskControllers.js");

console.log(taskControllers);

// Create task routes
router.post("/", taskControllers.createTask);

// View all tasks route
router.get("/allTasks", taskControllers.getAllTaskController);

router.get("/getSingleTask/:id", taskControllers.getSingleTaskController)

// Update a task status
router.patch("/updateTask/:id", taskControllers.updateTasksStatusController);

router.delete("/deleteTask/:id", taskControllers.deleteTask);

module.exports = router;